# FormArc Aurorae Themes
Aurorae themes for KDE Plasma 5.13+                                          

## How to install

```
git clone https://gitlab.com/sira313/formarc
```

```
cd formarc && mv FormArc ~/.local/share/aurorae/themes && cd .. && rm -rf formarc
```

**FormArc**

![screenshot](https://cn.pling.com/img/a/5/1/3/1cc11dec8daa963885d4aea7e81b51c5011d.png)
